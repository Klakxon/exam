package runtimeValidator;

import javax.validation.constraints.NotNull;

public class User {
    @NotNull
    @ValidEmail
    private String email;

    // Конструктор, геттери та сеттери
    public User(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}



